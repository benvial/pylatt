#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

import matplotlib.pyplot as plt

import pylatt as pl

plt.ion()
plt.close("all")
bk = pl.backend

l = 1
c0 = 1
d0 = 0.02
m0 = 2
J0 = 1


N = 10
alpha = 0.3

x = bk.linspace(0, 1, N)
y = bk.linspace(0, 1, N)
x, y = bk.meshgrid(x, y) + (0.5 - bk.random.rand(N, N)) * 2 * alpha / N

nodes = []

for i in range(N):
    for j in range(N):
        x0 = x[i, j]
        y0 = y[i, j]
        m0, J0 = bk.random.rand(2)
        nodes.append(pl.Node((x0, y0), m0, J0))


beams = []


for i in range(N):
    for j in range(N - 1):
        x0 = x[i, j]
        y0 = y[i, j]
        c0, d0 = bk.random.rand(2)
        beam = pl.Beam((x0, y0), (x[i, j + 1], y[i, j + 1]), c0, d0)
        beams.append(beam)
for i in range(N - 1):
    for j in range(N):
        x0 = x[i, j]
        y0 = y[i, j]
        c0, d0 = bk.random.rand(2)
        beam = pl.Beam((x0, y0), (x[i + 1, j], y[i + 1, j]), c0, d0)
        beams.append(beam)


lattice = pl.FrameFinite(nodes, beams)

w, v = lattice.eigensolve()
imode = 51
sol = v[:, imode]
omega = w[imode]

plt.figure(figsize=(2, 2))
lattice.animate(
    omega,
    sol,
    filename="doc/_static/_assets/mode.gif",
    scale=0.2,
    duration=50,
    l0=0.035,
)
