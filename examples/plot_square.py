#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

"""
Square finite lattice
======================

A simple example of a finite lattice
"""


import matplotlib.pyplot as plt

import pylatt as pl

######################################################################
# Define parameters, nodes and beams

m = 1
k = 1

nodes = [
    pl.Node(coordinates=(0, 0), mass=m),
    pl.Node(coordinates=(0, 1), mass=m),
    pl.Node(coordinates=(1, 1), mass=m),
    pl.Node(coordinates=(1, 0), mass=m),
]
beams = [
    pl.Beam(start=(0, 0), end=(0, 1), axial_stiffness=k),
    pl.Beam(start=(0, 1), end=(1, 1), axial_stiffness=k),
    pl.Beam(start=(1, 1), end=(1, 0), axial_stiffness=k),
    pl.Beam(start=(1, 0), end=(0, 0), axial_stiffness=k),
]


######################################################################
# Define the structure

lattice = pl.LatticeFinite(nodes=nodes, beams=beams)
plt.figure(figsize=(4, 4))
lattice.plot()


######################################################################
# Eigensolve!

omegas, modes = lattice.eigensolve()


######################################################################
# Plot the spectrum

plt.figure()
plt.plot(omegas, "o")
plt.ylabel(r"eigenfrequency $\omega$")
plt.xlabel("index")
plt.tight_layout()


######################################################################
# Plot the modes deformed shape

fig, ax = plt.subplots(2, 4, figsize=(7, 4))
ax = ax.ravel()
for imode, (o, mode) in enumerate(zip(omegas, modes.T)):
    plt.sca(ax[imode])
    lattice.plot_deform(omegas, mode, scale=0.2)
    plt.title(rf"$\omega_{{{imode}}}=${o:.3f}")
    plt.pause(0.01)
plt.tight_layout()


######################################################################
# Plot animation

for imode, (o, mode) in enumerate(zip(omegas, modes.T)):
    lattice.animate(
        o,
        mode,
        scale=0.2,
        filename=f"mode_{imode}.gif",
    )
    plt.close()
