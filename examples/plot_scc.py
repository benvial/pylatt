#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

"""
Simple cubic lattice
====================

Calculation of the band diagram of a three-dimensional simple cubic lattice.
"""


######################################################################
# Checking results from :cite:p:`Lucklum2018`.


import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend
pi = bk.pi


################################################
# Analytical solution


def ana(ks, k, m):
    return (4 * k / m) ** 0.5 * (bk.sin(a * ks / 2))


######################################################################
# Define parameters

a = 1  # lattice constant
R = 0.79 * a / 2  # ball diameter
r = 0.05 * a / 2  # circular beam diameter
E = 3.2e9  # elastic modulus
rho = 1190  # density
c = 2077.5  # longitudinal sound velocity
m = rho * (4 / 3 * pi * R**3 + 3 * (a - 2 * R) * pi * R**2)
k = E * pi * r**2 / (a - 2 * R * bk.cos(bk.arcsin(r / R)))

######################################################################
# Define nodes

nodes = []
n = pl.Node([0, 0, 0], m)
nodes.append(n)

######################################################################
# Define beams

beams = []
b = pl.Beam(nodes[0], (0, 0, a), k)
beams.append(b)
b = pl.Beam(nodes[0], (0, a, 0), k)
beams.append(b)
b = pl.Beam(nodes[0], (a, 0, 0), k)
beams.append(b)

######################################################################
# Define the lattice

basis_vectors = [a, 0, 0], [0, a, 0], [0, 0, a]
lattice = pl.Lattice(basis_vectors, nodes, beams, space_dim=3)

################################################
# Plot the unit cell

plt.figure()
lattice.plot(nper=3)

################################################
# Define the wavevector path

q = pi / a

Gamma = 0, 0, 0
X = 0, q, 0
M = q, q, 0
R = q, q, q

sym_points = Gamma, X, M, Gamma, R, [X, M], R
nbands = 51


k_bands = pl.init_bands_3D(sym_points, nbands)


k_bands_plot, marks = pl.init_bands_plot_3D(sym_points, nbands)

################################################
# Compute the bands

bd = lattice.compute_bands(k_bands, vectors=False)
bd_ana = ana(k_bands, k, m)

################################################
# Plot the band diagram

plt.figure(figsize=(12, 5))
lpl = plt.plot(k_bands_plot, bd * a / (2 * pi * c), "-", c="#3bc293", lw=3, alpha=0.5)
lana = plt.plot(k_bands_plot, bd_ana * a / (2 * pi * c), "--", c="#394541", lw=1)
plt.legend([lpl[0], lana[0]], ["pylatt", "analytical"])
plt.ylabel(r"$\omega$")
ticks = [r"$\Gamma$", r"$X$", r"$M$", r"$\Gamma$", r"$R$", r"$X/M$", r"$R$"]
for mark in marks:
    plt.axvline(mark, 0, bk.max(bd.real) * 1.1, c="k", lw=1.0)
plt.ylim(0)
plt.xlim(0, k_bands_plot[-1])
plt.xticks(marks, ticks)
plt.tight_layout()
