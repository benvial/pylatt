#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt


"""
Chiral medium
===================================

Calculation of the band diagram of a lattice with gyroscopic spinners.
"""

import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend

pi = bk.pi

######################################################################
# Checking results from :cite:p:`Carta2017`.

l = 1
q = 3**0.5 * l / 2
basis_vectors = [2 * l, 0], [l / 2, q]


######################################################################
# Build wavector path

nk = 801
kx = bk.linspace(-2 * pi, 2 * pi, nk)
ky = bk.linspace(-2 * pi / 3**0.5, 2 * pi / 3**0.5, nk)


######################################################################
# Function to build the lattice with pylatt


def build_lattice(l, m, c, alpha0, alpha1):
    # define the nodes
    node0 = pl.Node((0, 0), m, gyricity=alpha0)
    node1 = pl.Node((l, 0), m, gyricity=alpha1)
    nodes = [node0, node1]

    # define the beams
    beam0 = pl.Beam((0, 0), (l, 0), c)
    beam1 = pl.Beam((l, 0), (2 * l, 0), c)
    beam2 = pl.Beam((0, 0), (l / 2, q), c)
    beam3 = pl.Beam((l, 0), (3 * l / 2, q), c)
    beam4 = pl.Beam((l, 0), (l / 2, q), c)
    beam5 = pl.Beam((0, 0), (-l / 2, q), c)
    beams = [beam0, beam1, beam2, beam3, beam4, beam5]

    # instanciate the lattice object
    lattice = pl.Lattice(basis_vectors, nodes, beams)
    return lattice


######################################################################
# Let's compute and plot the band diagrams


def scalar_product(M, u, v):
    return (M @ u).conj() @ v.T


def get_berry_curvature(kx, ky, eigenmode, M):
    nkx = len(kx)
    nky = len(ky)

    phi = bk.empty((nkx - 1, nky - 1), dtype=bk.float64)
    for i in range(nkx - 1):
        for j in range(nky - 1):
            # print(i, j)
            u1 = eigenmode[i, j + 1]
            u2 = eigenmode[i + 1, j + 1]
            u3 = eigenmode[i + 1, j]
            u4 = eigenmode[i, j]
            p1 = scalar_product(M, u1, u2)
            p2 = scalar_product(M, u2, u3)
            p3 = scalar_product(M, u3, u4)
            p4 = scalar_product(M, u4, u1)
            q = p1 * p2 * p3 * p4
            phi[i, j] = -bk.log(q).imag

    ds = (kx[1] - kx[0]) * (ky[1] - ky[0])
    dkx = bk.array([kx[i + 1] - kx[i] for i in range(len(kx) - 1)])
    dky = bk.array([ky[i + 1] - ky[i] for i in range(len(ky) - 1)])
    return phi * dkx * dky


def get_chern_number(kx, ky, berry_curvature):
    dkx = bk.array([kx[i + 1] - kx[i] for i in range(len(kx) - 1)])
    dky = bk.array([ky[i + 1] - ky[i] for i in range(len(ky) - 1)])
    C = -bk.sum(berry_curvature / (dkx * dky)) / (2 * pi)
    return C


##########################

m = 1
c = 1

eps = 1e-8
alphas0 = [0.4, 0.4, 1.2, -1]
alphas1 = [0.8, 1.6, 1.6, 1]

for i in range(4):
    alpha0 = (alphas0[i] - eps) * m
    alpha1 = (alphas1[i] + eps) * m

    fig, ax = plt.subplots(1, 2)

    lattice = build_lattice(l, m, c, alpha0, alpha1)

    ks = bk.vstack([kx, 0 * ky]).T
    eigenvalues_kx = lattice.compute_bands(ks)

    ks = bk.vstack([0 * kx, ky]).T
    eigenvalues_ky = lattice.compute_bands(ks)

    ax[0].plot(kx, eigenvalues_kx)
    ax[0].set_ylim(0)
    ax[0].set_xlim(kx[0], kx[-1])
    ax[0].set_xlabel("$k_x$")
    ax[0].set_ylabel(r"$\omega$")
    ax[1].plot(ky, eigenvalues_ky)
    ax[1].set_ylim(0)
    ax[1].set_xlim(ky[0], ky[-1])
    ax[1].set_xlabel("$k_y$")
    ax[1].set_ylabel(r"$\omega$")
    plt.suptitle(rf"$\alpha_0={{{alphas0[i]}}},\alpha_1={{{alphas1[i]}}}$")
    plt.tight_layout()


# ##########################


# def build_lattice_square(l, m, c, alpha0, alpha1):
#     # define the nodes
#     node0 = pl.Node((0, 0), m, gyricity=alpha0)
#     node1 = pl.Node((l / 2, 0), m, gyricity=alpha1)
#     node2 = pl.Node((0, l / 2), m, gyricity=alpha1)
#     nodes = [node0, node1, node2]
#     # nodes = [node0, node1]

#     # define the beams
#     beam0 = pl.Beam((0, 0), (l / 2, 0), c)
#     beam1 = pl.Beam((l / 2, 0), (l, 0), c)
#     beam2 = pl.Beam((0, 0), (0, l / 2), c)
#     beam3 = pl.Beam((0, l / 2), (0, l), c)
#     beam4 = pl.Beam((0, 0), (0, l), c)
#     beams = [beam0, beam1, beam2, beam3]
#     # beams = [beam0, beam1, beam4]
#     basis_vectors = (l, 0), (0, l)
#     # instanciate the lattice object
#     lattice = pl.Lattice(basis_vectors, nodes, beams)
#     return lattice


# nk = 51
# kx = ky = bk.linspace(0, 2*pi / l, nk)
# lattice = build_lattice_square(l, m, c, 0.5, 0.6)
# M = lattice.mass_matrix()

# neig = lattice.dim
# eigenvalues = bk.empty((nk, nk, neig), dtype=bk.complex128)
# eigenmodes = bk.empty((nk, nk, lattice.dim, neig), dtype=bk.complex128)
# for i in range(nk):
#     _mode = []
#     for j in range(nk):
#         k =  kx[i], ky[j]
#         eigs, modes = lattice.eigensolve(k, vectors=True)
#         eigenvalues[i, j] = eigs
#         eigenmodes[i, j] = modes


# for imode in [4]:#range(neig):
#     mode = eigenmodes[:, :, :, imode]
#     phi = get_berry_curvature(kx, ky, mode, M)
#     C = get_chern_number(kx, ky, phi)
#     print(f"Mode {imode+1}: Chern number = {C}")

#     plt.figure()
#     plt.pcolormesh(kx, ky, phi)
#     plt.axis("scaled")
#     plt.title(f"Berry curvature, mode {imode+1}, C={C:.4}")
#     plt.colorbar()
#     plt.tight_layout()
#     plt.pause(0.001)


# ax = plt.figure().add_subplot(projection="3d")

# kx1, ky1 = bk.meshgrid(kx, ky)
# for n in range(0, neig):
#     # Plot the 3D surface
#     ev = eigenvalues[:, :, n].real
#     ax.plot_surface(
#         kx1, ky1, ev, edgecolor="none", lw=0.5, rstride=len(kx), cstride=len(ky), alpha=0.3
#     )
#     ax.set(xlabel="$k_x$", ylabel="$k_y$", zlabel="$\omega_n$")
#     plt.show()
