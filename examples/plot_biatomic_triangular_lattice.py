#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt


"""
Triangular biatomic lattice (truss)
===================================

Calculation of the band diagram of a two-dimensional phononic crystal.
"""


######################################################################
# Checking results from :cite:p:`Martinsson2003`.

import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend


######################################################################
# Define parameters

l = 1
q = 3**0.5 * l / 2
basis_vectors = [2 * l, 0], [l / 2, q]


######################################################################
# Special points

b0 = bk.pi / l
Gamma_point = 0, 0
M_point = b0, -0.5 * b0 / 3**0.5
X_point = b0, 1 * b0 / 3**0.5
sympoints = [Gamma_point, X_point, M_point]


######################################################################
# Build wavector path (and equivalent for plotting)

nband = 51
ks = pl.init_bands(sympoints, nband)
ksplot, kdist = pl.init_bands_plot(sympoints, nband)


######################################################################
# Analytical solution


class LatticeAnalytical(pl.Lattice):
    def __init__(self, l, m0, m1, c):
        self.l = l
        self.m0 = m0
        self.m1 = m1
        self.c = c
        self.progress = False

    def stiffness_matrix(self, k):
        l = self.l
        c = self.c
        n_dof = 2
        N = n_dof * 2
        phi = 0.25 * (k[0] * l + 3**0.5 * k[1] * l)
        psi1 = 0.5 * (3 * k[0] * l - 3**0.5 * k[1] * l)
        psi2 = 2 * phi  # 0.5 * (k[0] * l + 3**0.5 * k[0] * l)
        sp2 = bk.sin(phi) ** 2
        tmp = 3**0.5 / 2 - 3**0.5 * sp2
        sigma11 = bk.array([[5 / 2 + sp2, tmp], [tmp, 3 / 2 + 3 * sp2]]) * c
        tmp = -(3**0.5) / 4 * (bk.exp(-1j * psi1) + bk.exp(-1j * psi2))
        sigma12 = (
            bk.array(
                [
                    [
                        -1
                        - bk.exp(-2 * 1j * k[0] * l)
                        - 1 / 4 * bk.exp(-1j * psi1)
                        - 1 / 4 * bk.exp(-1j * psi2),
                        tmp,
                    ],
                    [tmp, -3 / 4 * (bk.exp(-1j * psi1) + bk.exp(-1j * psi2))],
                ]
            )
            * c
        )
        sigma = bk.zeros((N, N), dtype=bk.complex128)
        sigma[0:2, 0:2] = sigma11
        sigma[2:4, 2:4] = sigma11
        sigma[0:2, 2:4] = sigma12
        sigma[2:4, 0:2] = bk.conj(sigma12).T
        return sigma

    def eigensolve(self, k, vectors=False, rbme=None):
        sigma = self.stiffness_matrix(k)
        M = bk.diag(bk.array([self.m0, self.m0, self.m1, self.m1], dtype=bk.complex128))
        if vectors:
            o2, modes = pl.core.eig(sigma, M, vectors=vectors)
        else:
            o2 = pl.core.eig(sigma, M, vectors=vectors)
        o2 = bk.where(bk.abs(o2) < 1e-12, 0.0, o2)
        evals = o2.real**0.5
        ievals = bk.argsort(evals)
        if vectors:
            return evals[ievals], modes[:, ievals]
        else:
            return evals[ievals]


######################################################################
# Function to build the lattice with pylatt


def build_lattice(l, m0, m1, c):
    # define the nodes
    node0 = pl.Node((0, 0), m0)
    node1 = pl.Node((l, 0), m1)
    nodes = [node0, node1]

    # define the beams
    beam0 = pl.Beam((0, 0), (l, 0), c)
    beam1 = pl.Beam((l, 0), (2 * l, 0), c)
    beam2 = pl.Beam((0, 0), (l / 2, q), c)
    beam3 = pl.Beam((l, 0), (3 * l / 2, q), c)
    beam4 = pl.Beam((l, 0), (l / 2, q), c)
    beam5 = pl.Beam((0, 0), (-l / 2, q), c)
    beams = [beam0, beam1, beam2, beam3, beam4, beam5]

    # instanciate the lattice object
    lattice = pl.Lattice(basis_vectors, nodes, beams)
    return lattice


######################################################################
# Let's compute and plot the band diagram

m0 = 1
c = 1

fig, ax = plt.subplots(1, 2, figsize=(10, 6))

for i, m1 in enumerate([1, 10]):
    lattice = build_lattice(l, m0, m1, c)
    eigenvalues = lattice.compute_bands(ks)

    lattice_ana = LatticeAnalytical(l, m0, m1, c)
    eigenvalues_ana = lattice_ana.compute_bands(ks)

    plt.sca(ax[i])
    pl.plot_bands(
        sympoints,
        nband,
        eigenvalues,
        ls="-",
        color="#ba4545",
        lw=4,
        alpha=0.6,
    )
    ax[i].plot(ksplot, eigenvalues_ana, "--", color="#3e3e3e")
    lines = ax[i].get_lines()
    ax[i].legend([lines[0], lines[-1]], ["Numerical", "Analytical"])
    ax[i].set_title(f"$m_0={m0}$, $m_1={m1}$, $c_0=c_1={c}$")
    ax[i].set_ylim(0)
plt.tight_layout()


######################################################################
# Plot the lattice

plt.figure(figsize=(6, 3))
lattice.plot(nper=(3, 3))
plt.tight_layout()
