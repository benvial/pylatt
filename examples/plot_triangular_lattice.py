#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt


"""
Triangular lattice (truss)
==========================

Calculation of the band diagram of a two-dimensional phononic crystal.
"""

######################################################################
# Checking results from :cite:p:`Martinsson2003`.

import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend

######################################################################
# Define the parameters

l = 1
m0 = 1
c0 = 1

######################################################################
# Define the lattice

q = 3**0.5 * l / 2
basis_vectors = [l, 0], [l / 2, q]


node0 = pl.Node((0, 0), m0)
beam0 = pl.Beam((0, 0), (l, 0), c0)
beam1 = pl.Beam((0, 0), (l / 2, q), c0)
beam2 = pl.Beam((0, 0), (-l / 2, q), c0)
beams = [beam0, beam1, beam2]

nodes = [node0]
lattice = pl.Lattice(basis_vectors, nodes, beams)


######################################################################
# Plot the lattice

plt.figure(figsize=(6, 3))
lattice.plot(nper=(6, 3))
plt.tight_layout()

######################################################################
# Special points

b0 = bk.pi / l
Gamma_point = 0, 0
X_point = 2 * b0 / 3, 2 * b0 / 3**0.5
M_point = 0, 2 * b0 / 3**0.5
sympoints = [Gamma_point, X_point, M_point]


######################################################################
# Build wavector path

nband = 51
ks = pl.init_bands(sympoints, nband)

######################################################################
# Compute the band diagram

eigenvalues = lattice.compute_bands(ks)

######################################################################
# Plot bands

pl.plot_bands(sympoints, nband, eigenvalues)
plt.ylim(0)
plt.tight_layout()
