#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

"""
Square frame
==========================

Bi-atomic lattice.
"""

import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend

######################################################################
# Define parameters

l = 1

c0 = 1
c1 = 2
d0 = 0.02
d1 = 0.04
m0 = 2
m1 = 3
J0 = 1
J1 = 2


######################################################################
# Define nodes, beams and lattice

node0 = pl.Node((0, 0), m0, J0)
node1 = pl.Node((l, 0), m1, J1)
nodes = [node0, node1]

beam0 = pl.Beam((0, 0), (l, 0), c0, d0)
beam1 = pl.Beam((0, 0), (0, l), c0, d0)
beam2 = pl.Beam((l, 0), (2 * l, 0), c1, d1)
beam3 = pl.Beam((l, 0), (l, l), c1, d1)
beams = [beam0, beam1, beam2, beam3]

basis_vectors = [2 * l, 0], [l, l]

lattice = pl.Lattice(basis_vectors, nodes, beams, case="frame")

######################################################################
# Special points

b0 = bk.pi / (2 * l)
A_point = b0, 0
B_point = 0, 0
C_point = b0, b0
sympoints = [B_point, A_point, C_point]

######################################################################
# Build wavector path (and equivalent for plotting)

nband = 51
ks = pl.init_bands(sympoints, nband)
ksplot, kdist = pl.init_bands_plot(sympoints, nband)
eigenvalues = lattice.compute_bands(ks)


######################################################################
# Analytical solution


class LatticeAnalytical(pl.Lattice):
    def __init__(self, l, masses, moments, axial_stiffnesses, bending_stiffnesses):
        super().__init__(
            lattice.basis_vectors, lattice.nodes, lattice.beams, lattice.case
        )
        self.axial_stiffnesses = axial_stiffnesses
        self.bending_stiffnesses = bending_stiffnesses
        self.masses = masses
        self.moments = moments
        self.l = l

    def stiffness_matrix(self, k):
        k1, k2 = k
        l = self.l
        c1, c2 = self.axial_stiffnesses
        d1, d2 = self.bending_stiffnesses
        s = c1 + c2 + d1 + d2
        t = (d2 - d1) / 2
        u = 2 * (d2 + d1) / 3
        sigma11 = bk.array([[s, 0, t], [0, s, -t], [t, -t, u]])
        sigma22 = bk.array([[s, 0, -t], [0, s, t], [-t, t, u]])
        e = lambda x: bk.exp(x)
        sigma12_11 = (
            -c1
            - c2 * e(2j * k1 * l)
            - d1 * e(1j * (k1 - k2) * l)
            - d2 * e(1j * (k1 + k2) * l)
        )

        sigma12_13 = -0.5 * d1 * e(1j * (k1 - k2) * l) + 0.5 * d2 * e(
            1j * (k1 + k2) * l
        )

        sigma12_22 = (
            -c1 * e(1j * (k1 - k2) * l)
            - c2 * e(1j * (k1 + k2) * l)
            - d1
            - d2 * e(2j * k1 * l)
        )
        sigma12_23 = 0.5 * d1 - 0.5 * d2 * e(2j * k1 * l)
        sigma12_31 = 0.5 * d1 * e(1j * (k1 - k2) * l) - 0.5 * d2 * e(1j * (k1 + k2) * l)

        sigma12_32 = -0.5 * d1 + 0.5 * d2 * e(2j * k1 * l)

        sigma12_33 = 1 / 6 * d1 * (1 + e(1j * (k1 - k2) * l)) + 1 / 6 * d2 * (
            e(2j * k1 * l) + e(1j * (k1 + k2) * l)
        )

        sigma12 = bk.array(
            [
                [sigma12_11, 0, sigma12_13],
                [0, sigma12_22, sigma12_23],
                [sigma12_31, sigma12_32, sigma12_33],
            ]
        )

        _sigma_mat = bk.stack(
            [bk.stack([sigma11, sigma12]), bk.stack([sigma12.T.conj(), sigma22])]
        )

        sigma = bk.zeros((self.dim, self.dim), dtype=bk.complex128)
        for i in range(self.num_nodes):
            for j in range(self.num_nodes):
                sigma[
                    self.n_dof * i : self.n_dof * (i + 1),
                    self.n_dof * j : self.n_dof * (j + 1),
                ] = _sigma_mat[i, j]
        return sigma


lattice_ana = LatticeAnalytical(l, [m0, m1], [J0, J1], [c0, c1], [d0, d1])


eigenvalues_ana = lattice_ana.compute_bands(ks)

assert bk.allclose(eigenvalues, eigenvalues_ana)


######################################################################
# Let's plot the band diagram


plt.figure(figsize=(7, 7))

pl.plot_bands(
    sympoints,
    nband,
    eigenvalues,
    ls="-",
    color="#ba4545",
    lw=4,
    alpha=0.6,
    xtickslabels=[r"$A$", r"$B$", r"$C$", r"$A$"],
)
plt.plot(ksplot, eigenvalues_ana, "--k")
lines = plt.gca().get_lines()
plt.legend([lines[0], lines[-1]], ["Numerical", "Analytical"])
plt.ylim(0)
plt.title(
    f"$m_0={m0}$, $J_0={J0}$, $c_0={c0}$, $d_0={d0}$\n$m_1={m1}$, $J_1={J1}$, $c_1={c1}$, $d_1={d1}$"
)
plt.tight_layout()


######################################################################
# Plot the lattice

plt.figure(figsize=(6, 3))
lattice.plot(nper=(3, 3))
plt.tight_layout()
