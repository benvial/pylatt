#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

"""
Triangular lattice with oscillator
==================================

Calculation of the band diagram of a two-dimensional phononic crystal.
"""

######################################################################
# Checking results from :cite:p:`Martinsson2003`.

import matplotlib.pyplot as plt

import pylatt as pl

bk = pl.backend


######################################################################
# Define parameters

l = 1
m0 = 1
c0 = 1

q = 3**0.5 * l / 2

basis_vectors = [l, 0], [l / 2, q]


node0 = pl.Node((0, 0), m0)
beam0 = pl.Beam((0, 0), (l, 0), c0)
beam1 = pl.Beam((0, 0), (l / 2, q), c0)
beam2 = pl.Beam((0, 0), (-l / 2, q), c0)
beams = [beam0, beam1, beam2]

nodes = [node0]
lattice = pl.Lattice(basis_vectors, nodes, beams)


nband = 51

b0 = bk.pi / l
Gamma_point = 0, 0
X_point = b0, 1 * b0 / 3**0.5
M_point = b0, -0.5 * b0 / 3**0.5
sympoints = [Gamma_point, X_point, M_point]
nband = 51
ks = pl.init_bands(sympoints, nband)
ksplot, kdist = pl.init_bands_plot(sympoints, nband)


eigenvalues0 = lattice.compute_bands(ks)


################################################
# Plot the unit cell

plt.figure()
lattice.plot()


################################################
# Simple oscillator


fig, ax = plt.subplots(1, 3, figsize=(7.5, 4))

plt.sca(ax[0])
pl.plot_bands(
    sympoints,
    nband,
    eigenvalues0,
    ls="-",
    color="#ba4545",
)
plt.title(f"$m_0={m0}$, $c_0={c0}$", fontsize="small")
plt.tight_layout()
plt.ylim(0, 2.9)

h = q / 3
b = q - h

for iax, (m1, c1) in enumerate(zip([1, 4], [1, 0.25])):
    node1 = pl.Node((l / 2, h), m1)
    beam3 = pl.Beam((0, 0), (l / 2, h), c1)
    beam4 = pl.Beam((l / 2, h), (l / 2, q), c1)
    beam5 = pl.Beam((l / 2, h), (l, 0), c1)

    beams = [beam0, beam1, beam2, beam3, beam4, beam5]

    nodes = [node0, node1]
    lattice = pl.Lattice(basis_vectors, nodes, beams)

    eigenvalues = lattice.compute_bands(ks)
    plt.sca(ax[iax + 1])
    pl.plot_bands(
        sympoints,
        nband,
        eigenvalues,
        ls="-",
        color="#ba4545",
    )
    plt.title(f"$m_0={m0}$, $m_1={m1}$, $c_0={c0}$, $c_1={c1}$", fontsize="small")
    plt.ylim(0, 2.9)
    plt.tight_layout()


################################################
# Plot the unit cell

plt.figure()
lattice.plot()


######################################################################
# Plot animation

eigenvalues, modes = lattice.eigensolve(X_point)
for imode, (ev, mode) in enumerate(zip(eigenvalues, modes.T)):
    lattice.animate(
        X_point,
        ev,
        mode,
        nper=3,
        filename=f"mode_tri_{imode}.gif",
        scale=0.15,
    )
    plt.close()

################################################
# Complex oscillator


h = q / 3

p = 0.15 / 2 * l
deltay = 3**0.5 / 4 * p
deltax = p / 2
y0 = h - deltay
y1 = h + deltay
x0 = l / 2 - deltax
x1 = l / 2 + deltax

m0 = 1
c0 = 1
m1 = 1
c1 = 0.2

node0 = pl.Node((0, 0), m0)

node1 = pl.Node((l / 2, h - deltay), m1)
node2 = pl.Node((l / 2 + deltax, h + deltay), m1)
node3 = pl.Node((l / 2 - deltax, h + deltay), m1)

nodes = [node0, node1, node2, node3]
b = (y0**2 + l**2 / 4) ** 0.5

beam3 = pl.Beam((0, 0), (l / 2, h - deltay), c1)
beam4 = pl.Beam((0, 0), (l / 2 - deltax, h + deltay), c1)
beam5 = pl.Beam((l, 0), (l / 2, h - deltay), c1)
beam6 = pl.Beam((l, 0), (l / 2 + deltax, h + deltay), c1)
beam7 = pl.Beam((l / 2, q), (l / 2 + deltax, h + deltay), c1)
beam8 = pl.Beam((l / 2, q), (l / 2 - deltax, h + deltay), c1)
beam9 = pl.Beam((l / 2 + deltax, h + deltay), (l / 2 - deltax, h + deltay), c1)
beam10 = pl.Beam((l / 2 + deltax, h + deltay), (l / 2, h - deltay), c1)
beam11 = pl.Beam((l / 2 - deltax, h + deltay), (l / 2, h - deltay), c1)

beams = [
    beam0,
    beam1,
    beam2,
    beam3,
    beam4,
    beam5,
    beam6,
    beam7,
    beam8,
    beam9,
    beam10,
    beam11,
]

lattice = pl.Lattice(basis_vectors, nodes, beams)
eigenvalues = lattice.compute_bands(ks)

################################################
# Plot the unit cell

plt.figure()
lattice.plot()


################################################
# Plot the bands

fig, ax = plt.subplots(1, 1, figsize=(6, 6))

pl.plot_bands(
    sympoints,
    nband,
    eigenvalues,
    ls="-",
    color="#ba4545",
)
plt.title(f"$m_0={m0}$, $m_1={m1}$, $c_0={c0}$, $c_1={c1}$")
plt.ylim(0)
plt.tight_layout()
