.. _examples-index:

Examples
========

Typical examples of application of the package are presented here. 
You can run them live in your browser on `mybinder.org <https://mybinder.org/v2/gl/bvial%pylatt.gitlab.io/doc?filepath=notebooks>`_.


.. .. only:: html

..   .. include:: ../_assets/binder_badge.rst
  
  

..   .. raw:: html

..    <p>
..    </p>

