import numpy as np
import pytest

from pylatt import Beam, Lattice, Node


def test_init_default_values():
    node = Node([0, 0, 0], 1.0)
    assert np.array_equal(node.coordinates, np.array([0, 0, 0], dtype=np.float64))
    assert node.mass == 1.0
    assert node.moment == 0
    assert node.gyricity == 0


def test_init_custom_values():
    node = Node([1, 2, 3], 2.0, moment=1.5, gyricity=0.5)
    assert np.array_equal(node.coordinates, np.array([1, 2, 3], dtype=np.float64))
    assert node.mass == 2.0
    assert node.moment == 1.5
    assert node.gyricity == 0.5


def test_init_invalid_values():
    with pytest.raises(ValueError):
        Node([1, 2, 3], -1.0)

    with pytest.raises(ValueError):
        Node([1, 2, 3], 1.0, moment=-1.5)


def test_copy_node():
    # Create a node
    node = Node([0, 0, 0], mass=1.0, moment=2.0, gyricity=3.0)

    # Create a copy of the node
    node_copy = node.copy()

    # Check that the attributes are the same
    assert np.all(node_copy.coordinates == node.coordinates)
    assert node_copy.mass == node.mass
    assert node_copy.moment == node.moment
    assert node_copy.gyricity == node.gyricity

    # Check that the original node and the copy are not the same object
    assert node_copy is not node


def test_copy_node_no_errors():
    # Create a node
    node = Node([0, 0, 0], mass=1.0, moment=2.0, gyricity=3.0)

    # Create a copy of the node
    node_copy = node.copy()

    # Check that no errors were raised
    assert True


def test_init_node_objects():
    start_node = Node([0, 0, 0], mass=1.0)
    end_node = Node([1, 1, 1], mass=1.0)
    beam = Beam(start_node, end_node, axial_stiffness=100)
    assert np.array_equal(beam.start, start_node.coordinates)
    assert np.array_equal(beam.end, end_node.coordinates)
    assert beam.axial_stiffness == 100
    assert beam.bending_stiffness == 0


def test_init_coordinate_arrays():
    start_coords = [0, 0, 0]
    end_coords = [1, 1, 1]
    beam = Beam(start_coords, end_coords, axial_stiffness=100)
    assert np.array_equal(beam.start, start_coords)
    assert np.array_equal(beam.end, end_coords)
    assert beam.axial_stiffness == 100
    assert beam.bending_stiffness == 0


def test_default_bending_stiffness():
    start_node = Node([0, 0, 0], mass=1.0)
    end_node = Node([1, 1, 1], mass=1.0)
    beam = Beam(start_node, end_node, axial_stiffness=100)
    assert beam.bending_stiffness == 0


def test_beam_copy_not_same_object():
    beam = Beam([0, 0, 0], [1, 0, 0], axial_stiffness=100)
    beam_copy = beam.copy()
    assert beam is not beam_copy


def test_beam_copy_same_attributes():
    beam = Beam([0, 0, 0], [1, 0, 0], axial_stiffness=100)
    beam_copy = beam.copy()
    assert np.all(beam_copy.start == beam.start)
    assert np.all(beam_copy.end == beam.end)
    assert beam_copy.axial_stiffness == beam.axial_stiffness
    assert beam_copy.bending_stiffness == beam.bending_stiffness


def test_beam_copy_independent_modification():
    beam = Beam([0, 0, 0], [1, 0, 0], axial_stiffness=100)
    beam_copy = beam.copy()
    beam_copy.axial_stiffness = 200
    assert beam.axial_stiffness == 100
    assert beam_copy.axial_stiffness == 200


def test_init_valid_inputs():
    basis_vectors = [[1, 0], [0, 1]]
    nodes = [Node((0, 0), 1)]
    beams = [Beam(nodes[0], (1, 1), 1)]
    lattice = Lattice(basis_vectors, nodes, beams)
    assert lattice.space_dim == 2
    assert lattice.max_members is None
    assert lattice.beams == beams
    assert np.array_equal(
        lattice.basis_vectors, np.array(basis_vectors, dtype=np.float64)
    )
    assert lattice.case == "truss"
    assert lattice.progress is False
    assert lattice.members == lattice._find_members()


def test_init_invalid_space_dim():
    basis_vectors = [[1, 0], [0, 1]]
    nodes = [Node((0, 0), 10)]
    beams = [Beam((0, 0), (1, 1), 1)]
    with pytest.raises(NotImplementedError):
        Lattice(basis_vectors, nodes, beams, space_dim=4)


def test_init_invalid_case_space_dim_1():
    basis_vectors = [[1, 0], [0, 1]]
    nodes = [Node((0, 0), 10)]
    beams = [Beam((0, 0), (1, 1), 1)]
    with pytest.raises(NotImplementedError):
        Lattice(basis_vectors, nodes, beams, space_dim=1, case="frame")


def test_init_valid_case_space_dim_2_and_3():

    basis_vectors = [[1, 0], [0, 1]]
    nodes = [Node((0, 0), 10)]
    beams = [Beam((0, 0), (1, 1), 1)]
    lattice = Lattice(basis_vectors, nodes, beams, space_dim=2, case="truss")
    assert lattice.n_dof == 2
    basis_vectors = [[1, 0, 0], [0, 1, 1], [1, 2, 3]]
    nodes = [Node((0, 0, 1), 10), Node((0, 1, 1), 1)]
    beams = [Beam((0, 0, 1), (1, 1, 1), 1)]
    lattice = Lattice(basis_vectors, nodes, beams, space_dim=3, case="truss")
    assert lattice.n_dof == 3
