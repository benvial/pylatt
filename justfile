
PROJECT_NAME := "pylatt"

BRANCH := "$(git branch --show-current)"

PROJECT_DIR := "$(realpath $PWD)"

VERSION := """$(python3 -c "from configparser import ConfigParser; p = ConfigParser(); p.read('setup.cfg'); print(p['metadata']['version'])")"""


GITLAB_PROJECT_ID := "46732169"

# Echo information
info:
    @echo {{PROJECT_NAME}} version {{VERSION}}, on branch {{BRANCH}}
    @echo directory {{PROJECT_DIR}}


# List recipes
list:
    just -l

# Make conda environment
conda-env:
    mamba env create -f environment.yml

# Install the python package locally in editable mode
install:
    pip install -e .

# Install development dependencies
dev:
    pip install -r dev/requirements.txt
# which conda && conda install -c conda-forge just || echo conda not found
# which just || echo just not found: see installation instructions here https://github.com/casey/just


# Install test dependencies
test-req:
    pip install -r test/requirements.txt

# Install documentation dependencies
doc-req:
    pip install -r doc/requirements.txt


# Build animation for doc landing page
anim:
    python dev/make_landing_anim.py

# Build html documentation (only updated examples)
doc: 
    cd doc && make -s html
    just postpro-doc

# Postprocess html documentation 
postpro-doc: 
    cd doc && make -s postpro

# Build html documentation (live reload)  
livedoc:   
    sphinx-autobuild -a doc doc/_build/html --watch examples/ --watch doc/_templates/ \
    --watch doc/_static/  --port=8001 --open-browser --delay 1 \
    --re-ignore 'doc/examples/*'

# Show html documentation in the default browser
show:
    cd doc && make -s show

# Cleanup
clean:
    cd doc && make -s clean
    rm -rf build dist


# Generate documentation api
api:
    rm -rf doc/api/         
    sphinx-apidoc -o doc/api pylatt -f --implicit-namespaces


# Lint using flake8
lint:
	flake8 --exit-zero --ignore=E501 setup.py {{PROJECT_NAME}} test/*.py examples/

# Check for duplicated code
dup:
	pylint --exit-zero -f colorized --disable=all --enable=similarities {{PROJECT_NAME}}

# Reformat code
style:
	@isort .
	@black .

# Update header text
header:
	@cd dev && python update_header.py

# Run tests
test:
    @export MPLBACKEND=agg  && pytest ./test --cov={{PROJECT_NAME}} --cov-report term  --cov-report html --cov-report xml --durations=1


# Push to gitlab
gl:
    @git add -A
    @read -p "Enter commit message: " MSG; \
    git commit -a -m "$MSG"
    @git push origin {{BRANCH}}


# Show gitlab repository
repo:
	xdg-open https://gitlab.com/benvial/{{PROJECT_NAME}}


# Clean, reformat and push to gitlab
save: style gl


# Tag and push tags
tag: clean style
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@echo "Version v{{VERSION}}"
	# @git add -A
	# git commit -a -m "Publish v{{VERSION}}"
	# @git push origin {{BRANCH}}
	@git tag v{{VERSION}} || echo Ignoring tag since it already exists
	@git push --tags || echo Ignoring tag since it already exists on the remote

# Create a release
release:
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@gitlab project-release create --project-id {{GITLAB_PROJECT_ID}} \
	--name "version {{VERSION}}" --tag-name "v{{VERSION}}" --description "Released version {{VERSION}}"


# Create python package
package:
	@if [ "$(git rev-parse --abbrev-ref HEAD)" != "main" ]; then exit 1; fi
	@rm -f dist/*
	@python3 -m build --sdist --wheel .

# Upload to pypi
pypi: package
	@twine upload dist/*


