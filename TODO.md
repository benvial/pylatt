
## 3D examples 
@article{lucklumBandgapEngineeringThreedimensional2018,
  title = {Bandgap Engineering of Three-Dimensional Phononic Crystals in a Simple Cubic Lattice},
  author = {Lucklum, Frieder and Vellekoop, Michael J.},
  year = {2018},
  month = nov,
  journal = {Applied Physics Letters},
  volume = {113},
  number = {20},
  pages = {201902},
  publisher = {{American Institute of Physics}},
  issn = {0003-6951},
  doi = {10.1063/1.5049663},
  urldate = {2023-01-04},
}

## Damping and more examples
@article{jensenPhononicBandGaps2003,
  title = {Phononic Band Gaps and Vibrations in One- and Two-Dimensional Mass\textendash Spring Structures},
  author = {Jensen, J. S.},
  year = {2003},
  month = oct,
  journal = {Journal of Sound and Vibration},
  volume = {266},
  number = {5},
  pages = {1053--1078},
  issn = {0022-460X},
  doi = {10.1016/S0022-460X(02)01629-2},
  urldate = {2022-08-17},
}
