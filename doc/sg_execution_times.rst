
:orphan:

.. _sphx_glr_sg_execution_times:


Computation times
=================
**00:24.603** total execution time for 7 files **from all galleries**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_examples_plot_triangular_oscillator.py` (``../examples/plot_triangular_oscillator.py``)
     - 00:09.258
     - 171.9
   * - :ref:`sphx_glr_examples_plot_square.py` (``../examples/plot_square.py``)
     - 00:08.052
     - 174.9
   * - :ref:`sphx_glr_examples_plot_gyro.py` (``../examples/plot_gyro.py``)
     - 00:03.766
     - 130.1
   * - :ref:`sphx_glr_examples_plot_scc.py` (``../examples/plot_scc.py``)
     - 00:01.038
     - 132.0
   * - :ref:`sphx_glr_examples_plot_biatomic_triangular_lattice.py` (``../examples/plot_biatomic_triangular_lattice.py``)
     - 00:00.890
     - 134.5
   * - :ref:`sphx_glr_examples_plot_triangular_lattice.py` (``../examples/plot_triangular_lattice.py``)
     - 00:00.851
     - 143.0
   * - :ref:`sphx_glr_examples_plot_square_frame.py` (``../examples/plot_square_frame.py``)
     - 00:00.749
     - 133.6
