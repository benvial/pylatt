#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# This file is part of pylatt
# License: GPLv3
# See the documentation at benvial.gitlab.io/pylatt

import pylatt
from pylatt import *

bk = pylatt.backend

import matplotlib.pyplot as plt

plt.ion()
plt.close("all")
# example triangular lattice
l = 1

q = 3**0.5 * l / 2

basis_vectors = [l, 0], [l / 2, q]
m0 = 1
c0 = 1

h = q / 3

p = 0.4 / 2 * l
deltay = 3**0.5 / 4 * p
deltax = p / 2
y0 = h - deltay
y1 = h + deltay
x0 = l / 2 - deltax
x1 = l / 2 + deltax

m0 = 2
c0 = 1
m1 = 1
c1 = 0.2

# m1 = 4
# c1 = 0.25


# node0 = Node((0, 0), m0)

node0 = Node((l / 2, q), m0)
node1 = Node((l / 2, h - deltay), m1)
node2 = Node((l / 2 + deltax, h + deltay), m1)
node3 = Node((l / 2 - deltax, h + deltay), m1)

nodes = [node0, node1, node2, node3]

b = (y0**2 + l**2 / 4) ** 0.5

beam0 = Beam((0, 0), (l, 0), c0)
beam1 = Beam((0, 0), (l / 2, q), c0)
beam2 = Beam((l, 0), (l / 2, q), c0)

beam3 = Beam((0, 0), (l / 2, h - deltay), c1)
beam4 = Beam((0, 0), (l / 2 - deltax, h + deltay), c1)
beam5 = Beam((l, 0), (l / 2, h - deltay), c1)
beam6 = Beam((l, 0), (l / 2 + deltax, h + deltay), c1)
beam7 = Beam((l / 2, q), (l / 2 + deltax, h + deltay), c1)
beam8 = Beam((l / 2, q), (l / 2 - deltax, h + deltay), c1)
beam9 = Beam((l / 2 + deltax, h + deltay), (l / 2 - deltax, h + deltay), c1)
beam10 = Beam((l / 2 + deltax, h + deltay), (l / 2, h - deltay), c1)
beam11 = Beam((l / 2 - deltax, h + deltay), (l / 2, h - deltay), c1)

beams = [
    beam0,
    beam1,
    beam2,
    beam3,
    beam4,
    beam5,
    beam6,
    beam7,
    beam8,
    beam9,
    beam10,
    beam11,
]

lattice = Lattice(basis_vectors, nodes, beams)
eigenvalues = []


# k = kx, ky

# _eig, modes = lattice.eigensolve(k)
plt.figure(figsize=(2, 2))
lattice.plot(nper=(1, 1), wbnds=(0.4, 2), mbnds=(8, 16))
plt.ylim(-0.2, 1.1)
plt.tight_layout()

# plt.subplots_adjust(left=0., right=0., top=0., bottom=0.)
plt.savefig("_static/_assets/pylatt.svg", transparent=True)
plt.savefig("_static/_assets/pylatt.png", transparent=True)

plt.figure(figsize=(2, 2))
lattice.plot(nper=(1, 1), lc="w", wbnds=(0.4, 2), mbnds=(8, 16))
plt.ylim(-0.2, 1.1)
plt.tight_layout()

# plt.subplots_adjust(left=0., right=0., top=0., bottom=0.)
plt.savefig("_static/_assets/pylatt-dark.svg", transparent=True)
plt.savefig("_static/_assets/pylatt-dark.png", transparent=True)
